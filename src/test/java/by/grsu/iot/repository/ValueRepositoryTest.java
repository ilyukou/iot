package by.grsu.iot.repository;

import by.grsu.iot.entity.Value;
import by.grsu.iot.repository.interf.ValueRepository;
import by.grsu.iot.repository.jpa.ValueElasticsearchRepository;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ValueRepositoryTest {

    Long sensor = 1L;
    Long time = 2L;
    double value = 12.0;
    @Autowired
    private ValueRepository valueRepository;
    @MockBean
    private ValueElasticsearchRepository valueElasticsearchRepository;
    @MockBean
    private RestHighLevelClient restHighLevelClient;

    @Test
    public void save() {
        Value expected = new Value();
        expected.setSensor(sensor);
        expected.setTime(time);
        expected.setValue(value);


        when(valueElasticsearchRepository.save(expected)).thenReturn(expected);

        Value actual = valueRepository.save(expected);

        Assert.assertEquals(expected.getSensor(), actual.getSensor());
        Assert.assertEquals(expected.getTime(), actual.getTime());
        Assert.assertEquals(expected.getValue(), actual.getValue(), 0.0);
    }
}
