package by.grsu.iot.service;

import by.grsu.iot.entity.Sensor;
import by.grsu.iot.entity.Value;
import by.grsu.iot.exception.EntityNotFoundException;
import by.grsu.iot.factory.EntityFactory;
import by.grsu.iot.repository.interf.SensorRepository;
import by.grsu.iot.repository.interf.ValueRepository;
import by.grsu.iot.service.interf.ValueService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ValueServiceTest {

    String token = "token";
    Long time = 1L;
    Long sensorId = 1L;
    Double value = 10.0;
    Long from = 1L;
    Long to = 10L;
    @Autowired
    private ValueService valueService;
    @MockBean
    private SensorRepository sensorRepository;
    @MockBean
    private ValueRepository valueRepository;
    @MockBean
    private EntityFactory entityFactory;

    @Test
    public void add_whenSensorExist() {
        Sensor sensor = new Sensor();
        sensor.setId(sensorId);

        Value v = new Value();
        v.setId("id");
        v.setTime(time);
        v.setValue(value);

        when(sensorRepository.getByToken(token)).thenReturn(sensor);
        when(valueRepository.save(v)).thenReturn(v);
        when(entityFactory.createValue()).thenReturn(v);

        Value actual = valueService.add(token, time, value);

        Assert.assertEquals(v, actual);
    }

    @Test
    public void add_whenSensorNotExist() {
        Sensor sensor = new Sensor();
        sensor.setId(sensorId);

        when(sensorRepository.getByToken(token)).thenReturn(null);

        Assert.assertThrows(EntityNotFoundException.class, () -> valueService.add(token, time, value));
    }

    @Test
    public void getLast_whenSensorExist() throws IOException {
        Sensor sensor = new Sensor();
        sensor.setId(sensorId);

        Value v = new Value();

        when(sensorRepository.getByToken(token)).thenReturn(sensor);
        when(valueRepository.getLast(sensor.getId())).thenReturn(v);

        Assert.assertEquals(v, valueService.getLast(token));
    }

    @Test
    public void getLast_whenSensorNotExist() throws IOException {
        when(sensorRepository.getByToken(token)).thenReturn(null);

        Value v = new Value();
        when(valueRepository.getLast(sensorId)).thenReturn(v);

        Assert.assertThrows(EntityNotFoundException.class, () -> valueService.getLast(token));
    }

    @Test
    public void getValueByPeriod_whenSensorExist() throws IOException {
        Sensor sensor = new Sensor();
        sensor.setId(sensorId);

        List<Value> values = new ArrayList<>();

        when(sensorRepository.getByToken(token)).thenReturn(sensor);
        when(valueRepository.getByPeriod(sensor.getId(), from, to)).thenReturn(values);

        Assert.assertEquals(values, valueService.getValueByPeriod(token, from, to));
    }

    @Test
    public void getValueByPeriod_whenSensorNotExist() {
        when(sensorRepository.getByToken(token)).thenReturn(null);
        Assert.assertThrows(EntityNotFoundException.class, () -> valueService.getValueByPeriod(token, from, to));
    }

    @Test
    public void getAllValue_whenSensorExist() throws IOException {
        Sensor sensor = new Sensor();
        sensor.setId(sensorId);

        List<Value> values = new ArrayList<>();

        when(sensorRepository.getByToken(token)).thenReturn(sensor);
        when(valueRepository.getAll(sensor.getId())).thenReturn(values);

        Assert.assertEquals(values, valueService.getValueByPeriod(token, from, to));
    }

    @Test
    public void getAllValue_whenSensorNotExist() {
        when(sensorRepository.getByToken(token)).thenReturn(null);
        Assert.assertThrows(EntityNotFoundException.class, () -> valueService.getAllValue(token));
    }
}
