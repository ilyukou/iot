package by.grsu.iot.service;

import by.grsu.iot.entity.Email;
import by.grsu.iot.entity.User;
import by.grsu.iot.repository.interf.UserRepository;
import by.grsu.iot.service.interf.EmailService;
import by.grsu.iot.service.interf.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserServiceTest {

    private static final String correctEmail = "test@test.com";
    private static final String correctPassword = "Password123";
    private static final String correctUsername = "test";
    private static final String uncorrectEmail = "test@testcom";
    private static final String uncorrectUsername = "test";
    @Autowired
    private UserService userService;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private EmailService emailService;

    @Test
    public void create_whenUserWithSuchEmailNotExist() {

        User user = new User();
        Email email = new Email();
        email.setAddress(correctEmail);

        user.setEmail(email);

        when(userRepository.isExistByUsername(correctUsername)).thenReturn(false);
        when(emailService.isExist(correctEmail)).thenReturn(false);

        when(userRepository.create(user)).thenReturn(user);

        user.setPassword(correctPassword);
        user.setUsername(correctUsername);

        user = userService.create(user);

        Assert.assertEquals(correctPassword, user.getPassword());
        Assert.assertEquals(correctUsername, user.getUsername());
    }

    @Test
    public void create_whenUserWithSuchExistByUsername() {
        User user = new User();
        Email email = new Email();
        email.setAddress(correctEmail);

        user.setEmail(email);

        when(userRepository.isExistByUsername(correctUsername)).thenReturn(true);
        when(emailService.isExist(correctEmail)).thenReturn(false);


        when(userRepository.create(user)).thenReturn(user);

        user.setPassword(correctPassword);
        user.setUsername(correctUsername);

        Assert.assertThrows(IllegalArgumentException.class, () -> userService.create(user));
    }

    @Test
    public void create_whenUserWithSuchExistByEmail() {
        User user = new User();
        Email email = new Email();
        email.setAddress(correctEmail);

        user.setEmail(email);

        when(userRepository.isExistByUsername(correctUsername)).thenReturn(false);
        when(emailService.isExist(correctEmail)).thenReturn(true);

        when(userRepository.create(user)).thenReturn(user);

        user.setPassword(correctPassword);
        user.setUsername(correctUsername);

        Assert.assertThrows(IllegalArgumentException.class, () -> userService.create(user));
    }

    @Test
    public void create_whenUserWithSuchExistByEmailAndUsername() {
        User user = new User();
        Email email = new Email();
        email.setAddress(correctEmail);

        user.setEmail(email);

        when(userRepository.isExistByUsername(correctUsername)).thenReturn(false);
        when(emailService.isExist(correctEmail)).thenReturn(true);

        when(userRepository.create(user)).thenReturn(user);

        user.setPassword(correctPassword);
        user.setUsername(correctUsername);

        Assert.assertThrows(IllegalArgumentException.class, () -> userService.create(user));
    }
}
