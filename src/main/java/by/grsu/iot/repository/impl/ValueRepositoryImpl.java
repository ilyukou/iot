package by.grsu.iot.repository.impl;

import by.grsu.iot.entity.Value;
import by.grsu.iot.repository.interf.ValueRepository;
import by.grsu.iot.repository.jpa.ValueElasticsearchRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ValueRepositoryImpl implements ValueRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValueRepositoryImpl.class);

    private static final String INDEX = "value";

    private final ValueElasticsearchRepository valueElasticsearchRepository;
    private final RestHighLevelClient restHighLevelClient;
    private final ObjectMapper objectMapper;

    @Autowired
    public ValueRepositoryImpl(ValueElasticsearchRepository valueElasticsearchRepository,
                               RestHighLevelClient restHighLevelClient, ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        this.valueElasticsearchRepository = valueElasticsearchRepository;
        this.restHighLevelClient = restHighLevelClient;
    }

    @Override
    public Value save(Value value) {
        return valueElasticsearchRepository.save(value);
    }

    @Override
    public Value getLast(Long sensor) {

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(INDEX);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.size(1);

        sourceBuilder.sort(new FieldSortBuilder("time").order(SortOrder.DESC));

        sourceBuilder.query(QueryBuilders.termQuery("sensor", sensor));

        searchRequest.source(sourceBuilder);

        SearchResponse searchResponse = getSearchResponse(searchRequest, RequestOptions.DEFAULT);

        List<Value> values = getSearchResult(searchResponse);

        if (values.size() == 0) {
            return new Value();
        }

        if (values.size() == 1) {
            return values.get(0);
        }

        throw new IllegalArgumentException();
    }

    private List<Value> getSearchResult(SearchResponse searchResponse) {
        if (searchResponse == null) {
            return new ArrayList<>();
        }

        SearchHit[] searchHit = searchResponse.getHits().getHits();

        List<Value> values = new ArrayList<>();

        for (SearchHit hit : searchHit) {
            values
                    .add(objectMapper
                            .convertValue(hit
                                    .getSourceAsMap(), Value.class));
        }

        return values;
    }

    @Override
    public List<Value> getByPeriod(Long sensor, Long from, Long to) {

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(INDEX);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        sourceBuilder.sort(new FieldSortBuilder("time").order(SortOrder.ASC));

        QueryBuilder rangeQuery = QueryBuilders
                .rangeQuery("time")
                .from(from)
                .to(to)
                .includeLower(true)
                .includeUpper(true);

        sourceBuilder.query(QueryBuilders.boolQuery()
                .must(QueryBuilders.termQuery("sensor", sensor))
                .must(rangeQuery));

        searchRequest.source(sourceBuilder);

        return getSearchResult(getSearchResponse(searchRequest, RequestOptions.DEFAULT));
    }

    @Override
    public List<Value> getAll(Long sensor) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(INDEX);

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        sourceBuilder.query(QueryBuilders.termQuery("sensor", sensor));
        sourceBuilder.sort(new FieldSortBuilder("time").order(SortOrder.ASC));
        sourceBuilder.size(10000);
        searchRequest.source(sourceBuilder);

        return getSearchResult(getSearchResponse(searchRequest, RequestOptions.DEFAULT));
    }

    private SearchResponse getSearchResponse(SearchRequest searchRequest, RequestOptions requestOptions) {
        SearchResponse searchResponse;
        try {
            searchResponse = restHighLevelClient.search(searchRequest, requestOptions);
            return searchResponse;
        } catch (Exception e) {
            // FIXME - if SearchResponse size equals 0 - restHighLevelClient throw Exception
            return null;
        }
    }
}
