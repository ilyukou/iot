package by.grsu.iot.repository.impl;

import by.grsu.iot.entity.Project;
import by.grsu.iot.entity.Sensor;
import by.grsu.iot.entity.Status;
import by.grsu.iot.factory.EntityFactory;
import by.grsu.iot.repository.interf.SensorRepository;
import by.grsu.iot.repository.jpa.SensorJpaRepository;
import by.grsu.iot.service.interf.ProjectService;
import by.grsu.iot.util.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

@Repository
public class SensorRepositoryImpl implements SensorRepository {

    private static final Logger LOG = LoggerFactory.getLogger(SensorRepositoryImpl.class);

    private final SensorJpaRepository sensorJpaRepository;
    private final ProjectService projectService;
    private final TimeUtil timeUtil;

    public SensorRepositoryImpl(SensorJpaRepository sensorJpaRepository, ProjectService projectService, TimeUtil timeUtil) {
        this.sensorJpaRepository = sensorJpaRepository;
        this.projectService = projectService;
        this.timeUtil = timeUtil;
    }

    @Override
    public Sensor create(Project project, String name) {

        Sensor sensor = EntityFactory.createSensor();
        sensor.setName(name);
        sensor.setProject(project);

        Set<Sensor> sensors = new HashSet<>();

        if (project.getSensors() != null && project.getSensors().size() > 0) {
            sensors = project.getSensors();
        }

        sensors.add(sensor);
        project.setSensors(sensors);

        sensor = sensorJpaRepository.save(sensor);

        projectService.update(project);

        return sensor;
    }

    @Override
    public Sensor getById(Long id) {
        return sensorJpaRepository.findById(id).orElse(null);
    }

    @Override
    public Sensor getByToken(String token) {
        return sensorJpaRepository.findSensorByToken(token).orElse(null);
    }

    @Override
    public boolean disableSensorBySensorId(Long sensorId) {
        Sensor sensor = getById(sensorId);

        if (sensor == null) {
            return false;
        }

        sensor.setStatus(Status.DISABLED);

        update(sensor);

        return true;
    }

    @Override
    public boolean isExist(Long id) {
        return sensorJpaRepository.existsById(id);
    }

    @Override
    public Sensor update(Sensor sensor) {
        sensor.setUpdated(timeUtil.getCurrentDate());
        return sensorJpaRepository.save(sensor);
    }

    @Override
    public boolean isExist(String token) {
        return getByToken(token) != null;
    }
}
