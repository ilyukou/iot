package by.grsu.iot.repository.impl;

import by.grsu.iot.entity.Email;
import by.grsu.iot.entity.Status;
import by.grsu.iot.entity.User;
import by.grsu.iot.repository.interf.UserRepository;
import by.grsu.iot.repository.jpa.UserJpaRepository;
import by.grsu.iot.service.activemq.EmailProducer;
import by.grsu.iot.service.interf.EmailService;
import by.grsu.iot.util.TimeUtil;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final PasswordEncoder passwordEncoder;
    private final UserJpaRepository userJpaRepository;
    private final TimeUtil timeUtil;
    private final EmailService emailService;
    private final EmailProducer emailProducer;

    public UserRepositoryImpl(PasswordEncoder passwordEncoder, UserJpaRepository userJpaRepository,
                              TimeUtil timeUtil, EmailService emailService, EmailProducer emailProducer) {
        this.passwordEncoder = passwordEncoder;
        this.userJpaRepository = userJpaRepository;
        this.timeUtil = timeUtil;
        this.emailService = emailService;
        this.emailProducer = emailProducer;
    }

    @Override
    public User create(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        Email email = emailService.create(user.getEmail()); // Create Email entity

        user.setEmail(email); // set email with id

        user = userJpaRepository.save(user); // save user with email

        email.setUser(user); // insert user into email entity
        emailService.update(email); // save updates

        emailProducer.sendMessage(email.getAddress());

        return user;
    }

    @Override
    public User getById(Long id) {
        return userJpaRepository.findById(id).orElse(null);
    }

    @Override
    public boolean disableUserByUserId(Long userId) {
        User user = getById(userId);

        if (user == null) {
            return false;
        }

        user.setStatus(Status.DISABLED);

        update(user);

        return true;
    }

    @Override
    public boolean isExist(Long id) {
        return userJpaRepository.existsById(id);
    }

    @Override
    public User update(User user) {
        user.setUpdated(timeUtil.getCurrentDate());
        return userJpaRepository.save(user);
    }

    @Override
    public User getByUsername(String username) {
        return userJpaRepository.findByUsername(username).orElse(null);
    }

    @Override
    public boolean isExistByUsername(String username) {
        return userJpaRepository.findByUsername(username).isPresent();
    }

}
