package by.grsu.iot.repository.jpa;

import by.grsu.iot.entity.Sensor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SensorJpaRepository extends JpaRepository<Sensor, Long> {
    Optional<Sensor> findSensorByToken(String token);
}
