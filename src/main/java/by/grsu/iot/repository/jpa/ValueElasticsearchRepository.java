package by.grsu.iot.repository.jpa;

import by.grsu.iot.entity.Value;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ValueElasticsearchRepository extends ElasticsearchRepository<Value, String> {
}
