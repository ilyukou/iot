package by.grsu.iot.repository.jpa;

import by.grsu.iot.entity.Email;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface EmailJpaRepository extends JpaRepository<Email, Long> {
    Optional<Email> findEmailByAddress(String address);

    Optional<Email> findEmailByCode(String verificationCode);
}
