package by.grsu.iot.repository.jpa;

import by.grsu.iot.entity.ProjectElasticsearch;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface ProjectElasticsearchJpaRepository extends ElasticsearchRepository<ProjectElasticsearch, String> {
    List<ProjectElasticsearch> findByNameAndTitle(String name, String title);

    ProjectElasticsearch findByProjectId(Long projectId);
}
