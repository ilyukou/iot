package by.grsu.iot.repository.jpa;

import by.grsu.iot.entity.Role;
import by.grsu.iot.entity.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleJpaRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByRole(RoleType roleType);
}
