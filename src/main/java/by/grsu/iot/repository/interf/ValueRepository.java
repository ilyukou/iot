package by.grsu.iot.repository.interf;

import by.grsu.iot.entity.Value;

import java.util.List;

public interface ValueRepository {

    Value save(Value value);

    Value getLast(Long sensor);

    List<Value> getByPeriod(Long sensor, Long from, Long to);

    List<Value> getAll(Long sensor);
}
