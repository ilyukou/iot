package by.grsu.iot.repository.interf;

import by.grsu.iot.entity.Project;
import by.grsu.iot.entity.Sensor;

public interface SensorRepository {
    Sensor create(Project project, String name);

    Sensor getById(Long id);

    Sensor getByToken(String token);

    boolean disableSensorBySensorId(Long sensorId);

    boolean isExist(Long id);

    Sensor update(Sensor sensor);

    boolean isExist(String token);
}
