package by.grsu.iot.repository.interf;

import by.grsu.iot.entity.Role;
import by.grsu.iot.entity.RoleType;

public interface RoleRepository {
    Role getRoleByRoleType(RoleType roleType);

    Role getRoleOrCreate(RoleType roleType);

    Role create(RoleType roleType);
}
