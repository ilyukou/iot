package by.grsu.iot.repository.interf;

import by.grsu.iot.entity.User;

public interface UserRepository {

    User create(User user);

    User getById(Long id);

    boolean disableUserByUserId(Long userId);

    boolean isExist(Long id);

    User update(User user);

    User getByUsername(String username);

    boolean isExistByUsername(String username);
}

