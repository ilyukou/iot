package by.grsu.iot.dto;

import by.grsu.iot.entity.Sensor;

import java.util.LinkedList;
import java.util.List;

/**
 * DTO for {@link Sensor}
 */
public class SensorDto {

    private Long id;

    private Long project;

    private String name;

    private String currentState;

    private List<SensorStateDto> states = new LinkedList<>();

    private String token;

    public SensorDto() {
    }

    public SensorDto(Sensor sensor) {
        this.id = sensor.getId();
        this.name = sensor.getName();
        this.token = sensor.getToken();

        this.currentState = sensor.getCurrentState();

        for (String key : sensor.getStates().keySet()) {
            this.states.add(
                    new SensorStateDto(key, sensor.getStates().get(key))
            );
        }

        if (sensor.getProject() != null) {
            this.project = sensor.getProject().getId();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProject() {
        return project;
    }

    public void setProject(Long project) {
        this.project = project;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public List<SensorStateDto> getStates() {
        return states;
    }

    public void setStates(List<SensorStateDto> states) {
        this.states = states;
    }
}
