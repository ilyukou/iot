package by.grsu.iot.dto;

import by.grsu.iot.entity.Sensor;
import by.grsu.iot.entity.SensorState;

/**
 * {@link by.grsu.iot.controller.SensorStateController}
 */
public class SensorStateDto {

    private String key;
    private String value;

    public SensorStateDto(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public SensorStateDto(SensorState sensorState) {
        this.key = sensorState.getKey();
        this.value = sensorState.getValue();
    }

    public SensorStateDto(Sensor sensor) {
        this.key = sensor.getCurrentState();
        this.value = sensor.getStates().get(sensor.getCurrentState());
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
