package by.grsu.iot.dto;

import by.grsu.iot.entity.Project;
import by.grsu.iot.entity.ProjectElasticsearch;
import by.grsu.iot.entity.Sensor;

import java.util.List;
import java.util.stream.Collectors;

/**
 * DTO for {@link Project}
 */
public class ProjectDto {

    private Long id;

    private List<Long> sensors;

    private String name;

    private String title;

    private Long owner;

    public ProjectDto(Project project) {
        this.id = project.getId();
        this.name = project.getName();
        this.title = project.getTitle();

        if (project.getUser() != null) {
            this.owner = project.getUser().getId();
        }

        if (project.getSensors() != null) {
            this.sensors = project.getSensors().stream()
                    .filter(Sensor::isActive)
                    .map(Sensor::getId)
                    .collect(Collectors.toList());
        }
    }

    public ProjectDto(ProjectElasticsearch projectElasticsearch) {
        this.id = projectElasticsearch.getProjectId();
        this.name = projectElasticsearch.getName();
        this.title = projectElasticsearch.getTitle();
        this.owner = projectElasticsearch.getOwner();
    }

    public ProjectDto() {
    }

    public ProjectDto(Long id, List<Long> sensors, String name, String title, Long owner) {
        this.id = id;
        this.sensors = sensors;
        this.name = name;
        this.title = title;
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Long> getSensors() {
        return sensors;
    }

    public void setSensors(List<Long> sensors) {
        this.sensors = sensors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }
}
