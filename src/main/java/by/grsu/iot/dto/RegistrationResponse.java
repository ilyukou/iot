package by.grsu.iot.dto;

import by.grsu.iot.entity.User;

public class RegistrationResponse {

    private Long id;

    private String username;

    public RegistrationResponse(Long id, String username) {
        this.id = id;
        this.username = username;
    }

    public RegistrationResponse(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
    }

    public RegistrationResponse() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
