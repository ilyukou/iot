package by.grsu.iot.dto;

import by.grsu.iot.entity.Value;

/**
 * DTO for {@link Value}
 */
public class ValueDto {

    private String id;

    private Long time;

    private double value;

    public ValueDto() {
    }

    public ValueDto(String id, Long time, double value) {
        this.id = id;
        this.time = time;
        this.value = value;
    }

    public ValueDto(Value value) {
        this.id = value.getId();
        this.time = value.getTime();
        this.value = value.getValue();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
