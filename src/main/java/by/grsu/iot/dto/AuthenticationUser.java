package by.grsu.iot.dto;

/**
 * The model uses, after successful user authorization, to pass the authorization token
 */
public class AuthenticationUser {

    private String username;
    private String token;

    public AuthenticationUser(String username, String token) {
        this.username = username;
        this.token = token;
    }

    public AuthenticationUser() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
