package by.grsu.iot.controller;

import by.grsu.iot.dto.ProjectDto;
import by.grsu.iot.entity.Project;
import by.grsu.iot.service.interf.ProjectService;
import by.grsu.iot.util.ValidationUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This controller allows CRUD operation with {@link Project}.
 */
@CrossOrigin
@RestController
@RequestMapping("/project")
public class ProjectController {

    private final ProjectService projectService;
    private final ValidationUtil validationUtil;

    public ProjectController(ProjectService projectService, ValidationUtil validationUtil) {
        this.projectService = projectService;
        this.validationUtil = validationUtil;
    }

    @PostMapping
    public ResponseEntity<Long> create(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestBody ProjectDto projectDto) {

        validationUtil.isUnValidNameForProject(projectDto.getName());

        return new ResponseEntity<>(projectService.create(projectDto.getName(), userDetails.getUsername(), projectDto.getTitle()).getId(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(
            @AuthenticationPrincipal UserDetails userDetails,
            @PathVariable Long id,
            @RequestBody ProjectDto projectDto
    ) {
        validationUtil.isUnValidNameForProject(projectDto.getName());

        projectService.update(id, projectDto.getName(), userDetails.getUsername());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProjectDto> get(
            @AuthenticationPrincipal UserDetails userDetails,
            @PathVariable Long id
    ) {
        return new ResponseEntity<>(new ProjectDto(projectService.getById(id, userDetails.getUsername())), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(
            @AuthenticationPrincipal UserDetails userDetails,
            @PathVariable Long id
    ) {
        projectService.deleteById(id, userDetails.getUsername());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Long>> getAll(
            @AuthenticationPrincipal UserDetails userDetails
    ) {
        return new ResponseEntity<>(projectService.getProjectIdsByUsername(userDetails.getUsername()), HttpStatus.OK);
    }

}
