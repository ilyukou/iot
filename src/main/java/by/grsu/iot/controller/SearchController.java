package by.grsu.iot.controller;

import by.grsu.iot.dto.ProjectDto;
import by.grsu.iot.entity.Project;
import by.grsu.iot.entity.ProjectElasticsearch;
import by.grsu.iot.service.interf.SearchService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This controller allows search {@link Project} by {@link Project#name} and {@link Project#title}
 */
@CrossOrigin
@RestController
@RequestMapping("/search")
public class SearchController {

    private final SearchService searchService;

    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping
    public ResponseEntity<List<Long>> getAll(
            @RequestParam String query
    ) {
        if (query.length() < 3) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(
                searchService.search(query).stream()
                        .map(ProjectElasticsearch::getProjectId)
                        .collect(Collectors.toList()),
                HttpStatus.OK);
    }
}
