package by.grsu.iot.controller;

import by.grsu.iot.dto.ValueDto;
import by.grsu.iot.service.interf.ValueService;
import by.grsu.iot.util.TimeUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This controller allows add new {@link by.grsu.iot.entity.Value} and get last {@link by.grsu.iot.entity.Value},
 * get by period or get all {@link List<by.grsu.iot.entity.Value>}
 */
@CrossOrigin
@RestController
@RequestMapping("/value")
public class ValueController {

    private final ValueService valueService;
    private final TimeUtil timeUtil;

    public ValueController(ValueService valueService, TimeUtil timeUtil) {
        this.valueService = valueService;
        this.timeUtil = timeUtil;
    }

    @PostMapping("/{token}")
    public ResponseEntity<Void> add(
            @PathVariable String token,
            @RequestBody ValueDto dto
    ) {
        if (dto.getTime() == null) {
            dto.setTime(timeUtil.getCurrentDate().getTime());
        }
        valueService.add(token, dto.getTime(), dto.getValue());
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @GetMapping("/{token}")
    public ResponseEntity<ValueDto> getLast(
            @PathVariable String token
    ) throws IOException {
        return new ResponseEntity<>(new ValueDto(valueService.getLast(token)), HttpStatus.OK);
    }

    @GetMapping("/period/{token}")
    public ResponseEntity<List<ValueDto>> getValueByPeriod(
            @PathVariable String token,
            @RequestParam Long from,
            @RequestParam Long to
    ) throws IOException {
        return new ResponseEntity<>(
                valueService.getValueByPeriod(token, from, to)
                        .stream()
                        .map(ValueDto::new)
                        .collect(Collectors.toList()), HttpStatus.OK);
    }

    @GetMapping("/all/{token}")
    public ResponseEntity<List<ValueDto>> getAllValue(
            @PathVariable String token
    ) throws IOException {
        return new ResponseEntity<>(
                valueService.getAllValue(token)
                        .stream()
                        .map(ValueDto::new)
                        .collect(Collectors.toList()), HttpStatus.OK);
    }
}
