package by.grsu.iot.controller;

import by.grsu.iot.dto.SensorDto;
import by.grsu.iot.service.interf.SensorService;
import by.grsu.iot.util.ValidationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

/**
 * This controller allows CRUD operation with {@link by.grsu.iot.entity.Sensor}.
 */
@CrossOrigin
@RestController
@RequestMapping("/sensor")
public class SensorController {

    private static final Logger LOG = LoggerFactory.getLogger(SensorController.class);

    private final SensorService sensorService;
    private final ValidationUtil validationUtil;

    public SensorController(SensorService sensorService, ValidationUtil validationUtil) {
        this.sensorService = sensorService;
        this.validationUtil = validationUtil;
    }

    @PostMapping
    public ResponseEntity<Long> create(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestBody SensorDto dto
    ) {
        validationUtil.isUnValidNameForSensor(dto.getName());

        return new ResponseEntity<>(
                new SensorDto(sensorService.create(dto.getProject(), dto.getName(), userDetails.getUsername())).getId(),
                HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(
            @AuthenticationPrincipal UserDetails userDetails,
            @PathVariable Long id,
            @RequestBody SensorDto dto
    ) {
        validationUtil.isUnValidNameForSensor(dto.getName());

        sensorService.update(id, dto.getName(), dto.getCurrentState(), userDetails.getUsername());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SensorDto> get(
            @AuthenticationPrincipal UserDetails userDetails,
            @PathVariable Long id
    ) {
        return new ResponseEntity<>(new SensorDto(sensorService.getById(id, userDetails.getUsername())), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(
            @AuthenticationPrincipal UserDetails userDetails,
            @PathVariable Long id
    ) {
        sensorService.deleteById(id, userDetails.getUsername());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
