package by.grsu.iot.controller;


import by.grsu.iot.service.interf.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * This controller allows confirm {@link by.grsu.iot.entity.User}.
 */
@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/confirm")
    public ResponseEntity<String> update(
            @RequestParam String token
    ) {

        userService.confirmUser(token);

        return new ResponseEntity<>("You account was confirmed", HttpStatus.OK);
    }
}
