package by.grsu.iot.controller;

import by.grsu.iot.dto.*;
import by.grsu.iot.entity.User;
import by.grsu.iot.factory.EntityFactory;
import by.grsu.iot.security.jwt.JwtTokenProvider;
import by.grsu.iot.service.interf.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;

/**
 * This controller allows sing in and sing up {@link User}.
 */
@CrossOrigin
@RestController
@RequestMapping("/auth")
public class AuthController {

    private static final Logger LOG = LoggerFactory.getLogger(AuthController.class);

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;

    public AuthController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
    }

    @PostMapping("/signIn")
    public ResponseEntity<AuthenticationUser> signIn(@RequestBody AuthenticationRequest data) {

        try {
            UsernamePasswordAuthenticationToken authReq
                    = new UsernamePasswordAuthenticationToken(data.getUsername(), data.getPassword());
            authenticationManager.authenticate(authReq);

            User user = userService.getByUsername(data.getUsername());

            if (user == null) {
                throw new UsernameNotFoundException("Username " + data.getUsername() + "not found");
            }

            String token = jwtTokenProvider.createToken(data.getUsername(), user.getRoles());

            return ok(new AuthenticationUser(data.getUsername(), token));
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username/password supplied");
        }
    }

    @PostMapping("/signUp")
    public ResponseEntity<RegistrationResponse> singUp(@RequestBody RegistrationRequest data) {
        User user = EntityFactory.createUser();

        user.setEmail(EntityFactory.createEmail(data.getEmail()));
        user.setUsername(data.getUsername());
        user.setPassword(data.getPassword());

        user = userService.create(user);

        return ok(new RegistrationResponse(user));
    }
}
