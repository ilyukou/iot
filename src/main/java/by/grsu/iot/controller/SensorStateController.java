package by.grsu.iot.controller;

import by.grsu.iot.dto.HttpMessageEnum;
import by.grsu.iot.dto.HttpMessageWrapper;
import by.grsu.iot.entity.SensorState;
import by.grsu.iot.service.interf.SensorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.concurrent.ForkJoinPool;

/**
 * This controller return the state of the {@link by.grsu.iot.entity.Sensor} and allows to change it.
 */
@CrossOrigin
@RestController
@RequestMapping("/sensorState")
public class SensorStateController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SensorStateController.class);
    private final SensorService sensorService;
    @Value("${iot.http.long-polling.timeout}")
    private long TIME_OUT_IN_MILLIS;

    public SensorStateController(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    /**
     * This method works on the log polling principle. As soon as the sensor state is different from
     * the transmitted one, the updated state will be returned.
     *
     * @param token        sensor token
     * @param currentState the last state you know
     * @return {@link HttpMessageWrapper} which contains {@link HttpMessageEnum#ok} if the states do not match and
     * {@link HttpMessageEnum#info} when request expired.
     */
    @GetMapping("/{token}")
    public DeferredResult<ResponseEntity<HttpMessageWrapper<SensorState>>> getCurrentState(
            @PathVariable String token,
            @RequestParam String currentState
    ) {

        // Default time out response
        DeferredResult<ResponseEntity<HttpMessageWrapper<SensorState>>> deferredResult =
                new DeferredResult<>(TIME_OUT_IN_MILLIS,
                        new HttpMessageWrapper(
                                HttpMessageEnum.info,
                                "Time Out",
                                null)
                );

        deferredResult.onCompletion(() -> LOGGER.info("Processing complete"));

        ForkJoinPool.commonPool().submit(() -> {

            // Result
            ResponseEntity<HttpMessageWrapper<SensorState>> responseEntity = new ResponseEntity<>(
                    new HttpMessageWrapper<>(
                            HttpMessageEnum.ok,
                            "ok",
                            sensorService.getStateWhenDeviceStateNotEqualState(token, currentState)
                    ),
                    HttpStatus.OK
            );

            deferredResult.setResult(responseEntity);

        });
        return deferredResult;
    }

    @PutMapping("/{token}")
    public ResponseEntity<Void> changeCurrentState(
            @PathVariable String token,
            @RequestParam String state
    ) {
        sensorService.setCurrentState(token, state);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
