package by.grsu.iot.factory;

import by.grsu.iot.entity.*;
import by.grsu.iot.repository.interf.RoleRepository;
import by.grsu.iot.util.StringUtil;
import by.grsu.iot.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Basic setting of the object.
 */
@Component
public class EntityFactory {

    // BaseEntity default fields
    private static final Status DEFAULT_STATUS = Status.NOT_ACTIVE;

    // User default fields
    private static final RoleType DEFAULT_ROLE_TYPE = RoleType.User;

    // Project default fields
    private static final AccessType PROJECT_ACCESS_TYPE = AccessType.PRIVATE;
    private static final Status PROJECT_STATUS = Status.ACTIVE;

    private static final long EMAIL_VERIFICATION_CODE_LENGTH = 30;
    private static Long tokenLength;
    private static RoleRepository roleRepository;
    private static TimeUtil timeUtil;
    private static StringUtil stringUtil;
    @Value("${iot.sensor.token.length}")
    private Long tokenLen;

    private static BaseEntity createBaseEntity() {
        BaseEntity baseEntity = new BaseEntity();

        baseEntity.setStatus(DEFAULT_STATUS);

        Date date = timeUtil.getCurrentDate();

        baseEntity.setCreated(date);
        baseEntity.setUpdated(date);

        return baseEntity;
    }

    public static User createUser() {
        User user = new User(createBaseEntity());

        List<Role> roles = new ArrayList<>();
        roles.add(roleRepository.getRoleOrCreate(DEFAULT_ROLE_TYPE));
        user.setRoles(roles);

        return user;
    }

    public static Sensor createSensor(Status projectStatus) {
        Sensor sensor = new Sensor(createBaseEntity());

        sensor.setStatus(projectStatus);
        sensor.setToken(stringUtil.generateToken(tokenLength));

        // Default values
        sensor.putState("off", "off");
        sensor.putState("on", "on");

        // Default state
        sensor.setCurrentState(sensor.getStates().get("off"));

        return sensor;
    }

    public static Sensor createSensor() {
        return createSensor(Status.ACTIVE);
    }

    public static Project createProject() {
        Project project = new Project(createBaseEntity());

        project.setStatus(PROJECT_STATUS);
        project.setAccessType(PROJECT_ACCESS_TYPE);

        return project;
    }

    public static Email createEmail() {
        return new Email(createBaseEntity());
    }

    public static Email createEmail(String address) {
        Email email = createEmail();

        email.setAddress(address);
        email.setCode(stringUtil.generateString(EMAIL_VERIFICATION_CODE_LENGTH));

        return email;
    }

    @Autowired
    public void setSomeThing(RoleRepository roleRepository, TimeUtil timeUtil, StringUtil stringUtil) {
        tokenLength = tokenLen;
        EntityFactory.roleRepository = roleRepository;
        EntityFactory.timeUtil = timeUtil;
        EntityFactory.stringUtil = stringUtil;
    }

    public by.grsu.iot.entity.Value createValue() {
        by.grsu.iot.entity.Value value = new by.grsu.iot.entity.Value();
        value.setId(UUID.randomUUID().toString());
        return value;
    }
}
