package by.grsu.iot.entity;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "sensor")
public class Sensor extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "projectId")
    private Project project;

    private String name;

    private String token;

    private String currentState;

    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name = "name")
    @Column(name = "value")
    @CollectionTable(name = "sensor_states", joinColumns = @JoinColumn(name = "sensor_id"))
    private Map<String, String> states = new HashMap<>();

    public Sensor(Long id, Project project, String name, String token) {
        super(id);
        this.project = project;
        this.name = name;
        this.token = token;
    }

    public Sensor() {
    }

    public Sensor(BaseEntity baseEntity) {
        super(baseEntity);
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String state) {
        this.currentState = state;
    }

    public Map<String, String> getStates() {
        return states;
    }

    public void setStates(Map<String, String> states) {
        this.states = states;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sensor sensor = (Sensor) o;

        return super.getId().equals(sensor.getId());
    }

    @Override
    public String toString() {
        return "Sensor{" +
                "id=" + super.getId() +
                ", project=" + project +
                ", name='" + name + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

    public void putState(String key, String value) {
        this.states.put(key, value);
    }
}
