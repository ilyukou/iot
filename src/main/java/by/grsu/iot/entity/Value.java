package by.grsu.iot.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * Entity for storing data received from {@link Sensor}
 */
@Document(indexName = "value", type = "value")
public class Value {

    @Id
    @Field(type = FieldType.Text, store = true)
    private String id;

    private Long sensor;

    private long time;

    private double value;

    public Value(String id, Long sensor, long time, double value) {
        this.id = id;
        this.sensor = sensor;
        this.time = time;
        this.value = value;
    }

    public Value(Long sensor, long time, double value) {
        this.sensor = sensor;
        this.time = time;
        this.value = value;
    }

    public Value() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getSensor() {
        return sensor;
    }

    public void setSensor(Long sensor) {
        this.sensor = sensor;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Value value1 = (Value) o;

        if (time != value1.time) return false;
        if (Double.compare(value1.value, value) != 0) return false;
        if (!id.equals(value1.id)) return false;
        return sensor.equals(value1.sensor);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id.hashCode();
        result = 31 * result + sensor.hashCode();
        result = 31 * result + (int) (time ^ (time >>> 32));
        temp = Double.doubleToLongBits(value);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Value{" +
                "id='" + id + '\'' +
                ", sensor=" + sensor +
                ", time=" + time +
                ", value=" + value +
                '}';
    }
}
