package by.grsu.iot.service.impl;

import by.grsu.iot.entity.Sensor;
import by.grsu.iot.entity.Value;
import by.grsu.iot.exception.EntityNotFoundException;
import by.grsu.iot.factory.EntityFactory;
import by.grsu.iot.repository.interf.SensorRepository;
import by.grsu.iot.repository.interf.ValueRepository;
import by.grsu.iot.service.interf.ValueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

@Transactional
@Service
public class ValueServiceImpl implements ValueService {

    private static final Logger LOG = LoggerFactory.getLogger(ValueServiceImpl.class);

    private final ValueRepository valueRepository;
    private final SensorRepository sensorRepository;
    private final EntityFactory entityFactory;

    public ValueServiceImpl(ValueRepository valueRepository, SensorRepository sensorRepository,
                            EntityFactory entityFactory) {
        this.valueRepository = valueRepository;
        this.sensorRepository = sensorRepository;
        this.entityFactory = entityFactory;
    }

    @Override
    public Value add(String token, Long time, double value) {
        Sensor sensor = sensorRepository.getByToken(token);

        if (sensor == null) {
            String ms = "Sensor does not exist with given token {" + token + "}";
            LOG.info(ms);
            throw new EntityNotFoundException(ms);
        }

        Value v = entityFactory.createValue();
        v.setValue(value);
        v.setTime(time);
        v.setSensor(sensor.getId());

        return valueRepository.save(v);
    }

    @Override
    public Value getLast(String token) throws IOException {
        Sensor sensor = sensorRepository.getByToken(token);

        if (sensor == null) {
            String ms = "Sensor does not exist with given token {" + token + "}";
            LOG.info(ms);
            throw new EntityNotFoundException(ms);
        }

        return valueRepository.getLast(sensor.getId());
    }

    @Override
    public List<Value> getValueByPeriod(String token, Long from, Long to) throws IOException {
        Sensor sensor = sensorRepository.getByToken(token);

        if (sensor == null) {
            String ms = "Sensor does not exist with given token {" + token + "}";
            LOG.info(ms);
            throw new EntityNotFoundException(ms);
        }

        return valueRepository.getByPeriod(sensor.getId(), from, to);
    }

    @Override
    public List<Value> getAllValue(String token) throws IOException {
        Sensor sensor = sensorRepository.getByToken(token);

        if (sensor == null) {
            String ms = "Sensor does not exist with given token {" + token + "}";
            LOG.info(ms);
            throw new EntityNotFoundException(ms);
        }

        return valueRepository.getAll(sensor.getId());
    }
}
