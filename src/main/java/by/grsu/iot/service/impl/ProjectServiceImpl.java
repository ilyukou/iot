package by.grsu.iot.service.impl;

import by.grsu.iot.entity.Project;
import by.grsu.iot.entity.User;
import by.grsu.iot.entity.activemq.ActActiveMQ;
import by.grsu.iot.exception.EntityNotFoundException;
import by.grsu.iot.repository.interf.ProjectRepository;
import by.grsu.iot.service.activemq.EntityProducer;
import by.grsu.iot.service.interf.ProjectService;
import by.grsu.iot.service.interf.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class ProjectServiceImpl implements ProjectService {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectServiceImpl.class);

    private final UserService userService;
    private final ProjectRepository projectRepository;
    private final EntityProducer entityProducer;

    @Autowired
    public ProjectServiceImpl(UserService userService, ProjectRepository projectRepository,
                              EntityProducer entityProducer) {
        this.userService = userService;
        this.projectRepository = projectRepository;
        this.entityProducer = entityProducer;
    }

    @Override
    public Project create(String name, String username, String title) {

        if (name == null || username == null || title == null) {
            String ms = "Name or username must be not null";
            LOG.info(ms);
            throw new IllegalArgumentException(ms);
        }

        Project project = projectRepository.create(name, username, title);

        entityProducer.sendMessage(project, ActActiveMQ.CREATE);

        return project;
    }

    @Override
    public Project update(Project project) {
        return projectRepository.update(project);
    }

    @Override
    public void update(Long id, String name, String username) {
        Project project = getById(id, username);

        if (project == null) {
            String ms = "Project does not exist with given id {" + id + "}";
            LOG.info(ms);
            throw new EntityNotFoundException(ms);
        }

        if (!project.getUser().getUsername().equals(username)) {
            String ms = "Project does not belong this user with giver username {" + username + "}";
            LOG.info(ms);
            throw new IllegalArgumentException(ms);
        }

        project.setName(name);

        project = update(project);

        entityProducer.sendMessage(project, ActActiveMQ.UPDATE);
    }

    @Override
    public Project getById(Long id, String username) {
        Project project = projectRepository.getById(id);

        if (project == null) {
            String ms = "Project does not exist with given id {" + id + "}";
            LOG.info(ms);
            throw new EntityNotFoundException(ms);
        }

        if (!project.getUser().getUsername().equals(username)) {
            String ms = "Project does not belong this user with giver username {" + username + "}";
            LOG.info(ms);
            throw new IllegalArgumentException(ms);
        }

        return project;
    }

    @Override
    public boolean deleteById(Long id, String username) {
        Project project = projectRepository.getById(id);

        if (project == null) {
            return false;
        }

        if (!project.getUser().getUsername().equals(username)) {
            String ms = "Project does not belong this user with giver username {" + username + "}";
            LOG.info(ms);
            throw new IllegalArgumentException(ms);
        }


        boolean result = projectRepository.disableProjectByProjectId(id);

        entityProducer.sendMessage(project, ActActiveMQ.DELETE);

        return result;
    }

    @Override
    public List<Long> getProjectIdsByUsername(String username) {
        User user = userService.getByUsername(username);

        if (user == null) {
            String ms = "Not found User by username {" + username + "}";
            LOG.info(ms);
            throw new EntityNotFoundException(ms);
        }

        return projectRepository.getProjects(user.getId());
    }

    @Override
    public Project getById(Long projectId) {
        return projectRepository.getById(projectId);
    }
}
