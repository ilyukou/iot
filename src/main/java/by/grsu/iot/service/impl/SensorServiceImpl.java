package by.grsu.iot.service.impl;

import by.grsu.iot.entity.Project;
import by.grsu.iot.entity.Sensor;
import by.grsu.iot.entity.SensorState;
import by.grsu.iot.exception.EntityNotFoundException;
import by.grsu.iot.repository.interf.ProjectRepository;
import by.grsu.iot.repository.interf.SensorRepository;
import by.grsu.iot.service.interf.SensorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;

@Transactional
@Service
public class SensorServiceImpl implements SensorService {

    private static final Logger LOG = LoggerFactory.getLogger(SensorServiceImpl.class);

    private final SensorRepository sensorRepository;
    private final ProjectRepository projectRepository;

    @Autowired
    public SensorServiceImpl(SensorRepository sensorRepository, ProjectRepository projectRepository) {
        this.sensorRepository = sensorRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Sensor create(Long projectId, String name, String username) {
        Project project = projectRepository.getById(projectId);

        if (!project.getUser().getUsername().equals(username)) {
            String ms = "Project does not belong this user with giver username {" + username + "}";
            LOG.warn(ms);
            throw new IllegalArgumentException(ms);
        }

        return sensorRepository.create(project, name);
    }

    @Override
    public Sensor getById(Long id, String username) {
        Sensor sensor = sensorRepository.getById(id);

        if (sensor == null || !sensor.isActive()) {
            String ms = "Sensor does not exist with given id {" + id + "}";
            LOG.warn(ms);
            throw new EntityNotFoundException(ms);
        }

        // FIXME - add LAZY init from db
        if (!sensor.getProject().getUser().getUsername().equals(username)) {
            String ms = "Project does not belong this user with giver username {" + username + "}";
            LOG.warn(ms);
            throw new IllegalArgumentException(ms);
        }

        return sensor;
    }

    @Override
    public boolean deleteById(Long id, String username) {

        Sensor sensor = sensorRepository.getById(id);

        if (sensor == null || !sensor.isActive()) {
            String ms = "Sensor does not exist with given id {" + id + "}";
            LOG.warn(ms);
            throw new EntityNotFoundException(ms);
        }

        // FIXME - add LAZY init from db
        if (!sensor.getProject().getUser().getUsername().equals(username)) {
            String ms = "Project does not belong this user with giver username {" + username + "}";
            LOG.warn(ms);
            throw new IllegalArgumentException(ms);
        }

        return sensorRepository.disableSensorBySensorId(id);
    }

    @Override
    public void update(Long id, String name, String currentState, String username) {
        Sensor sensor = getById(id, username);

        if (sensor == null || !sensor.isActive()) {
            String ms = "Sensor does not exist with given id {" + id + "}";
            LOG.warn(ms);
            throw new EntityNotFoundException(ms);
        }

        // FIXME - add LAZY init from db
        if (!sensor.getProject().getUser().getUsername().equals(username)) {
            String ms = "Project does not belong this user with giver username {" + username + "}";
            LOG.warn(ms);
            throw new IllegalArgumentException(ms);
        }

        if (name != null) {
            sensor.setName(name);
        }

        if (currentState != null && sensor.getStates().containsKey(currentState)) {
            sensor.setCurrentState(name);
        }

        update(sensor);
    }

    @Override
    public Sensor update(Sensor sensor) {
        return sensorRepository.update(sensor);
    }

    @Override
    public void setCurrentState(String token, String state) {
        Sensor sensor = sensorRepository.getByToken(token);

        if (sensor != null && sensor.getStates().containsKey(state)) {
            sensor.setCurrentState(state);
            update(sensor);
        }
    }

    @Override
    public Sensor getByToken(String token) {
        return sensorRepository.getByToken(token);
    }

    // FIXME - How to wait result without while loop ?
    public SensorState getStateWhenDeviceStateNotEqualState(String token, String deviceState) {
        while (true) {
            Sensor sensor = getByToken(token);

            if (sensor == null) {
                LOG.info("Sensor is null");
                return null;
            }

            if (!sensor.getCurrentState().equals(deviceState)) {
                LOG.info("Device state not equal db state");
                return new SensorState(sensor);
            }

            try {
                TimeUnit.SECONDS.sleep(1l);
            } catch (InterruptedException e) {
                // ignore
            }
        }
    }
}
