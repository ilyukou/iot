package by.grsu.iot.service.impl;

import by.grsu.iot.entity.ProjectElasticsearch;
import by.grsu.iot.repository.interf.ProjectElasticsearchRepository;
import by.grsu.iot.service.interf.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private ProjectElasticsearchRepository projectElasticsearchRepository;

    @Override
    public List<ProjectElasticsearch> search(String query) {
        return projectElasticsearchRepository.search(query);
    }
}
