package by.grsu.iot.service.interf;

import by.grsu.iot.entity.ProjectElasticsearch;

import java.util.List;

public interface SearchService {
    List<ProjectElasticsearch> search(String query);
}
