package by.grsu.iot.service.interf;

import by.grsu.iot.entity.Value;

import java.io.IOException;
import java.util.List;

public interface ValueService {
    Value add(String token, Long time, double value);

    Value getLast(String token) throws IOException;

    List<Value> getValueByPeriod(String token, Long from, Long to) throws IOException;

    List<Value> getAllValue(String token) throws IOException;
}
