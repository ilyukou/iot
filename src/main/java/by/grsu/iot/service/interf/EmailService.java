package by.grsu.iot.service.interf;

import by.grsu.iot.entity.Email;

public interface EmailService {

    Email findByAddress(String address);

    Email create(Email email);

    Email getById(Long id);

    Email update(Email email);

    boolean isExist(String address);

    Email findByCode(String verificationCode);
}
