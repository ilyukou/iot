package by.grsu.iot.service.interf;

import by.grsu.iot.entity.Sensor;
import by.grsu.iot.entity.SensorState;

public interface SensorService {
    Sensor create(Long projectId, String name, String username);

    Sensor getById(Long id, String username);

    boolean deleteById(Long id, String username);

    void update(Long id, String name, String currentState, String username);

    Sensor update(Sensor sensor);

    void setCurrentState(String token, String state);

    Sensor getByToken(String token);

    SensorState getStateWhenDeviceStateNotEqualState(String token, String deviceState);
}
